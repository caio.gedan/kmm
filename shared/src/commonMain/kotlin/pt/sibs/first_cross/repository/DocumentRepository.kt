package pt.sibs.first_cross.repository

import pt.sibs.first_cross.domain.IRepository
import pt.sibs.first_cross.domain.model.Document

/**
 * Created by csilva at 06/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
interface IDocumentRepository : IRepository<Document> {
    suspend fun assignDocument(doc: Document)

    suspend fun getDocument(id: Int): Document

    suspend fun getDocuments(): List<Document>
}
//
//class DocumentRepository constructor(
//    private val cacheData: ICacheData<Document>,
//    private val docServices: IDocumentServices,
//    private val documentMapper: ApiDocumentMapper
//) : IDocumentRepository {
//
//    override suspend fun assignDocument(doc: Document) {
//        docServices.assignDocument(JsonBuilder.toJson(doc))
//    }
//
//    override suspend fun getDocState(id: Int): Document {
//        val doc = JsonBuilder.fromJson(docServices.getDocState(id), ApiDocument::class.java)
//        return documentMapper.map(doc)
//    }
//
//    override suspend fun getDocuments(): List<Document> {
//        TODO("Not yet implemented")
//    }
//
//    override fun addLocal(data: Document) {
//        cacheData.add(data)
//    }
//
//    override fun getLocalList(): Flow<List<Document>> = cacheData.getList()
//
//    override suspend fun getRemoteList(): List<Document> {
//        val cList =
//            JsonBuilder.fromJson(docServices.getDocuments(), ApiDocumentsResponse::class.java)
//        return cList.results.map { documentMapper.map(it) }
//    }
//
//    override suspend fun getFromRemote(id: Int): Document {
//        return documentMapper.map(ApiDocument(1, "", Status.IDLE))
//    }
//}

