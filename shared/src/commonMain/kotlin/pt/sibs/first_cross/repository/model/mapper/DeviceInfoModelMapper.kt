package pt.sibs.first_cross.repository.model.mapper

import pt.sibs.first_cross.domain.model.Device
import pt.sibs.first_cross.domain.model.backend.ApiDevice
import pt.sibs.first_cross.domain.model.map.Mapper

/**
 * Created by csilva at 06/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
class DeviceMapper : Mapper<Device, ApiDevice>() {

    override fun mapToApi(model: Device) = model.run {
        ApiDevice(
            systemFamily,
            systemVersion,
            deviceManufacturer,
            systemArchitecture,
            deviceModel,
            deviceId
        )
    }

    override fun mapFromApi(model: ApiDevice) =
        model.run {
            Device(
                systemFamily,
                systemVersion,
                deviceManufacturer,
                systemArchitecture,
                deviceModel,
                deviceId
            )
        }

}