package pt.sibs.first_cross.repository.model.mapper

import pt.sibs.first_cross.domain.model.Document
import pt.sibs.first_cross.domain.model.backend.ApiDocument
import pt.sibs.first_cross.domain.model.map.Mapper

/**
 * Created by csilva at 06/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
class ApiDocumentMapper : Mapper<Document, ApiDocument>() {
    override fun mapToApi(model: Document): ApiDocument {
        TODO("Not yet implemented")
    }

    override fun mapFromApi(model: ApiDocument): Document {
        TODO("Not yet implemented")
    }

}