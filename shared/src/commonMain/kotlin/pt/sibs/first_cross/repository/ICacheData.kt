package pt.sibs.first_cross.repository

import kotlinx.coroutines.flow.Flow

/**
 * Created by csilva at 06/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
interface ICacheData<T> {
    fun add(data: T)
    fun getList(): Flow<List<T>>
    fun getById(id: Int): Flow<T>
    fun removeById(id: Int)

    fun getLastId(): Int
}
