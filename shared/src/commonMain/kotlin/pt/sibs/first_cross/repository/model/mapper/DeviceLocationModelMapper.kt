package pt.sibs.first_cross.repository.model.mapper

import pt.sibs.first_cross.domain.model.DeviceLocation
import pt.sibs.first_cross.domain.model.backend.ApiDeviceLocation
import pt.sibs.first_cross.domain.model.map.Mapper

/**
 * Created by csilva at 06/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
class ApiDeviceLocationMapper : Mapper<DeviceLocation, ApiDeviceLocation>() {

    override fun mapToApi(model: DeviceLocation): ApiDeviceLocation {
        TODO("Not yet implemented")
    }

    override fun mapFromApi(model: ApiDeviceLocation): DeviceLocation {
        TODO("Not yet implemented")
    }
}