package pt.sibs.first_cross.services.main

import kotlinx.coroutines.flow.Flow
import pt.sibs.first_cross.domain.model.Document
import pt.sibs.first_cross.repository.ICacheData
import pt.sibs.first_cross.repository.model.mapper.ApiDocumentMapper
import pt.sibs.first_cross.utils.helper.JsonBuilder

/**
 * Created by csilva at 05/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */

/*
Flow ::
Em corrotinas, um fluxo é um tipo que pode emitir vários valores sequencialmente,
ao contrário das funções de suspensão, que retornam somente um valor.
Por exemplo, você pode usar um fluxo para receber atualizações em tempo real de um banco de dados.
https://developer.android.com/kotlin/flow?msclkid=2d639880cf8d11ecb46fcbc1a6b50118
 */
interface IDocumentServices{

    fun saveDBDocument(jsonDoc: String)
    fun removeDBDocument(id: Int)
    fun getDBDocument(id: Int): Flow<Document>
    fun getDBDocuments(): Flow<List<Document>>

    fun getLastId(): Int
}

class DocumentServices(
    private val cacheData: ICacheData<Document>,
    private val documentMapper: ApiDocumentMapper
) : IDocumentServices {

    override fun saveDBDocument(jsonDoc: String) {
//        val document = JsonBuilder.fromJson(jsonDoc, Document::class.java)
//        cacheData.add(document)
    }

    override fun getDBDocument(id: Int): Flow<Document> = cacheData.getById(id)

    override fun getDBDocuments(): Flow<List<Document>> = cacheData.getList()

    override fun getLastId(): Int = cacheData.getLastId()

    override fun removeDBDocument(id: Int) {
        cacheData.removeById(id)
    }

}