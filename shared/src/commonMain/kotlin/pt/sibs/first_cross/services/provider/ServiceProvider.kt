package pt.sibs.first_cross.services.provider

import android.util.Log
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import pt.sibs.first_cross.services.async_executor.ServiceAsyncExecutor
import pt.sibs.first_cross.services.base.IServiceContract
import pt.sibs.first_cross.utils.helper.JsonBuilder

/**
 * Created by csilva at 10/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
class ServiceProvider : IServiceProvider, ServiceAsyncExecutor() {

    init {
        registerAllServices()
    }

    override fun executeAsync(uri: String, req: String) = executeAsync(uri, req) {}

    override fun executeAsync(uri: String, req: String, callback: ((String) -> Unit)) {
        val block: suspend () -> IServiceContract.Response = {
            Log.i(TAG, "Service called Asynchronously: Uri: [$uri] and \n Request: [$req]")
            delay(5000)
            getService(uri).process()
        }

        launch(
            flow = flow {
                emit(block())
            },
            onSuccess = {
                when (it) {
                    is IServiceContract.Response.Success<*> ->
                        callback.invoke(convertToRequestModel(uri, req, it.value.toString()))
                    is IServiceContract.Response.Error -> callback.invoke(JsonBuilder.toJson(it.msg))
                }
            },
            onError = {
                callback.invoke("Error")
            }
        )
    }

    override fun executeSync(uri: String, req: String, callback: ((String) -> Unit)) {
        Log.i(TAG, "Service called Synchronously: Uri: [$uri] and \n Request: [$req]")
        when (val result = getService(uri).process()) {
            is IServiceContract.Response.Success<*> -> callback.invoke(
                convertToRequestModel(uri, req, result.value.toString()))
            else -> {}
        }
    }

    override fun executeSyncWithResponse(uri: String, req: String): String {
        Log.i(TAG, "Service called Synchronously: Uri: [$uri] and \n Request: [$req]")
        return when (val result = getService(uri).process()) {
            is IServiceContract.Response.Success<*> ->
                convertToRequestModel(uri, req, result.value.toString())
            else -> ""
        }
    }

    override fun executeSync(uri: String, req: String) = executeSync(uri, req) {}

    internal companion object {
        const val TAG = "ServiceProvider"
    }

}


