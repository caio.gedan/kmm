package pt.sibs.first_cross.services.provider

import org.koin.java.KoinJavaComponent.inject
import pt.sibs.first_cross.domain.model.Device
import pt.sibs.first_cross.domain.model.backend.ApiDevice
import pt.sibs.first_cross.repository.model.mapper.DeviceMapper
import pt.sibs.first_cross.services.base.IServiceContract
import pt.sibs.first_cross.services.main.DeviceServices.deviceInfo
import pt.sibs.first_cross.services.main.DeviceServices.deviceLocation
import pt.sibs.first_cross.utils.helper.JsonBuilder

/**
 * Created by csilva at 11/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */

private val serviceMap: HashMap<String, IServiceContract> = HashMap()
private val modelMap: HashMap<String, HashMap<String, ((String) -> String)>> = HashMap()

fun getService(uri: String): IServiceContract {

    if (!serviceMap.containsKey(uri))
        throw IllegalAccessException("Service $uri not implemented!")

    return serviceMap[uri]!!
}

fun convertToRequestModel(uri: String, model: String, toConvert: String): String {
    val mapper = getServiceModel(uri, model)
    return run { mapper.invoke(toConvert) }
}

fun registerAllServices() {
    registerDeviceServices()
}

private fun getServiceModel(uri: String, modelRequest: String): ((String) -> String) {
    if (!modelMap.containsKey(uri) || !modelMap[uri]!!.contains(modelRequest))
        throw IllegalArgumentException("")

    return modelMap[uri]?.get(modelRequest)!!
}

private fun registerService(uri: String, service: IServiceContract) =
    registerService(uri, service) {}

private fun registerService(
    uri: String,
    service: IServiceContract,
    registerModels: (HashMap<String, ((String) -> String)>) -> Unit,
) {
    serviceMap[uri] = service
    modelMap[uri] = HashMap()

    modelMap[uri]?.let { registerModels(it) }
}

private fun registerDeviceServices() {
    val deviceMapper: DeviceMapper by inject(DeviceMapper::class.java)

    registerService("device/info", deviceInfo) { models ->
        models[JsonBuilder.toJson(ApiDevice())] =
            {
                val device = JsonBuilder.fromJson(it, Device::class.java)
                val apiMapped = deviceMapper.mapToApi(device)
                JsonBuilder.toJson(apiMapped)
            }
    }

    registerService("device/location", deviceLocation)
}
