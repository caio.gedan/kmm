package pt.sibs.first_cross.services.base

/**
 * Created by csilva at 12/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */

interface IServiceContract {

    sealed class Response {
        data class Success<T>(val value: T) : Response()
        data class Error(val msg: String, val cause: Exception? = null) : Response()
    }

//    data class Request<T>(val arg: T) : IRequest
//    fun process(req: IRequest): Response

    fun process(): Response
}