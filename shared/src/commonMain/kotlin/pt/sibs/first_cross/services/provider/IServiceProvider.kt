package pt.sibs.first_cross.services.provider

/**
 * Created by csilva at 11/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
interface IServiceProvider {

    fun executeAsync(uri: String, req: String)
    fun executeAsync(uri: String, req: String, callback: ((String) -> Unit))

    fun executeSync(uri: String, req: String)
    fun executeSyncWithResponse(uri: String, req: String): String
    fun executeSync(uri: String, req: String, callback: ((String) -> Unit))

}