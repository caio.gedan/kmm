package pt.sibs.first_cross.utils.helper

/**
 * Created by csilva at 04/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
expect object JsonBuilder {

    fun toJson(pojo: Any): String
    fun <T> fromJson(json: String, type: Class<T>): T
}