package pt.sibs.first_cross.di

import kotlinx.coroutines.Dispatchers
import org.koin.core.context.startKoin
import org.koin.core.module.Module
import org.koin.dsl.KoinAppDeclaration
import org.koin.dsl.bind
import org.koin.dsl.module
import pt.sibs.first_cross.database.DocumentCacheImpl
import pt.sibs.first_cross.domain.interactors.document.AssignDocumentUseCase
import pt.sibs.first_cross.domain.interactors.document.GetDocumentUseCase
import pt.sibs.first_cross.domain.interactors.document.GetDocumentsUseCase
import pt.sibs.first_cross.domain.interactors.document.SaveDocumentUseCase
import pt.sibs.first_cross.domain.interactors.type.UseCaseIn
import pt.sibs.first_cross.domain.interactors.type.UseCaseInOut
import pt.sibs.first_cross.domain.interactors.type.UseCaseOutFlow
import pt.sibs.first_cross.repository.ICacheData
import pt.sibs.first_cross.repository.model.mapper.ApiDocumentMapper
import pt.sibs.first_cross.repository.model.mapper.DeviceMapper
import pt.sibs.first_cross.services.CppBridge
import pt.sibs.first_cross.services.main.DocumentServices
import pt.sibs.first_cross.services.main.IDocumentServices
import pt.sibs.first_cross.services.provider.IServiceProvider
import pt.sibs.first_cross.services.provider.ServiceProvider

/**
 * Created by csilva at 03/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */

/*
https://medium.com/android-news/koin-simple-android-di-a47827a707ce
 */
fun startDI(appDeclaration: KoinAppDeclaration = {}) =
    startKoin {
        appDeclaration()
        modules(
            listOf(
                repositoryModule,
                mapperModule,
                dispatcherModule,
                usesModule,
                generalServices,
                platformModule()
            )
        )
    }

//IoS
fun startDI() = startDI { }

private val generalServices =
    module {
        single { ServiceProvider() } bind IServiceProvider::class
        single { DocumentServices(get(), get()) } bind IDocumentServices::class
    }

private val mapperModule =
    module {
        factory { DeviceMapper() }
        factory { ApiDocumentMapper() }
    }

private val repositoryModule =
    module {
        single { DocumentCacheImpl(get()) } bind ICacheData::class
    }

private val usesModule: Module =
    module {
        factory { AssignDocumentUseCase(get(), get()) } bind UseCaseIn::class
        factory { GetDocumentUseCase(get()) } bind UseCaseInOut::class
        factory { GetDocumentsUseCase(get()) } bind UseCaseOutFlow::class
        factory { SaveDocumentUseCase(get()) } bind UseCaseIn::class
    }

private val dispatcherModule = module {
    factory { Dispatchers.Default }
}
