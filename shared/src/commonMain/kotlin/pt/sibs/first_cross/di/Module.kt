package pt.sibs.first_cross.di

import org.koin.core.module.Module

/**
 * Created by csilva at 03/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
expect fun platformModule(): Module