package pt.sibs.first_cross.features.document.mvi

import org.koin.core.component.inject
import pt.sibs.first_cross.base.mvi.BaseViewModel
import pt.sibs.first_cross.base.mvi.BasicUiState
import pt.sibs.first_cross.domain.interactors.document.GetDocumentsUseCase

/**
 * Created by csilva at 06/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
open class DocumentsViewModel :
    BaseViewModel<DocumentsContract.Event, DocumentsContract.State>() {
    private val getDocumentsUseCase: GetDocumentsUseCase by inject()

    init {
        getDocuments()
    }

    override fun createInitialState(): DocumentsContract.State =
        DocumentsContract.State(docListState = BasicUiState.Idle)

    override fun handleEvent(event: DocumentsContract.Event) {
        when (event) {
            DocumentsContract.Event.OnGetDocuments -> getDocuments()
        }
    }

    private fun getDocuments() {
        setState { copy(docListState = BasicUiState.Loading) }
        launch(
            flow = getDocumentsUseCase(),
            onSuccess = {
                setState {
                    copy(
                        docListState = if (it.isEmpty())
                            BasicUiState.Empty
                        else
                            BasicUiState.Success(it)
                    )
                }
            },
            onError = {
                setState { copy(docListState = BasicUiState.Error()) }
            })
    }

}