package pt.sibs.first_cross.features.document.mvi

import pt.sibs.first_cross.base.mvi.BasicUiState
import pt.sibs.first_cross.base.mvi.UiEvent
import pt.sibs.first_cross.base.mvi.UiState
import pt.sibs.first_cross.domain.model.Document

/**
 * Created by csilva at 06/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
interface DocumentDetailContract {
    sealed class Event : UiEvent {
        data class GetDocument(val id: Int) : Event()
        data class SaveDocument(val doc: Document) : Event()
        object AssignDocument : Event()
        object Retry : Event()
    }

    data class State(
        val docState: BasicUiState<Document>,
        val isAssigned: Boolean
    ) : UiState

}
