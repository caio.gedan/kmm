package pt.sibs.first_cross.features.document.mvi

import pt.sibs.first_cross.base.mvi.BasicUiState
import pt.sibs.first_cross.base.mvi.UiEvent
import pt.sibs.first_cross.base.mvi.UiState
import pt.sibs.first_cross.domain.model.Document

/**
 * Created by csilva at 07/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
interface DocumentsContract {
    sealed class Event : UiEvent {
        object OnGetDocuments : Event()
    }

    data class State(
        val docListState: BasicUiState<List<Document>>
    ) : UiState
}