package pt.sibs.first_cross.features.document.mvi

import org.koin.core.component.inject
import pt.sibs.first_cross.base.mvi.BaseViewModel
import pt.sibs.first_cross.base.mvi.BasicUiState
import pt.sibs.first_cross.domain.interactors.document.AssignDocumentUseCase
import pt.sibs.first_cross.domain.interactors.document.GetDocumentUseCase
import pt.sibs.first_cross.domain.interactors.document.SaveDocumentUseCase
import pt.sibs.first_cross.domain.model.Document
import pt.sibs.first_cross.domain.model.Status

/**
 * Created by csilva at 06/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
open class DocumentDetailViewModel :
    BaseViewModel<DocumentDetailContract.Event, DocumentDetailContract.State>() {

    // Can add others details CRUD use case here
    private val getDocumentUseCase: GetDocumentUseCase by inject()
    private val saveDocumentUseCase: SaveDocumentUseCase by inject()
    private val assignDocumentUseCase: AssignDocumentUseCase by inject()

    private var documentId: Int? = null
    private lateinit var document: Document

    override fun createInitialState(): DocumentDetailContract.State =
        DocumentDetailContract.State(
            docState = BasicUiState.Idle,
            isAssigned = false
        )

    override fun handleEvent(event: DocumentDetailContract.Event) {
        when (event) {
            is DocumentDetailContract.Event.GetDocument -> getDocument(event.id)
            is DocumentDetailContract.Event.SaveDocument -> saveDocument(event.doc)
            is DocumentDetailContract.Event.AssignDocument -> {
                assignDocument(Document(0, "simulate", Status.IDLE))
            }
            DocumentDetailContract.Event.Retry -> documentId?.let { getDocument(it) }
        }
    }

    private fun assignDocument(doc: Document) {
        setState { copy(docState = BasicUiState.Loading) }
        launch(
            flow = assignDocumentUseCase(doc),
            onSuccess = {
                setState { copy(isAssigned = true) }
            },
            onError = {
                setState { copy(docState = BasicUiState.Error("Error on Assign the document.")) }
            })
    }

    private fun saveDocument(doc: Document) {
        setState { copy(docState = BasicUiState.Loading) }
        launch(
            flow = saveDocumentUseCase(doc),
            onSuccess = {

            },
            onError = {
                setState { copy(docState = BasicUiState.Error()) }
            })
    }

    private fun getDocument(id: Int) {
        documentId = id
        setState { copy(docState = BasicUiState.Loading) }
        launch(
            flow = getDocumentUseCase(documentId!!),
            onSuccess = {
                document = it
                setState { copy(docState = BasicUiState.Success(it)) }
            },
            onError = {
                setState { copy(docState = BasicUiState.Error()) }
            }
        )
    }

}