package pt.sibs.first_cross.database

import com.squareup.sqldelight.runtime.coroutines.asFlow
import com.squareup.sqldelight.runtime.coroutines.mapToList
import com.squareup.sqldelight.runtime.coroutines.mapToOne
import kotlinx.coroutines.flow.Flow
import pt.sibs.first_cross.database.sqldelight.DatabaseDriverFactory
import pt.sibs.first_cross.domain.model.Document
import pt.sibs.first_cross.domain.model.Status
import pt.sibs.first_cross.repository.ICacheData

/**
 * Created by csilva at 06/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
class DocumentCacheImpl(
    databaseDriverFactory: DatabaseDriverFactory
) : ICacheData<Document> {

    private val dbQuery = database(databaseDriverFactory).sDKDatabaseQueries

    override fun add(data: Document) {
        dbQuery.transaction {
            dbQuery.insertDocument(
                data.name,
                data.status
            )
        }
    }

    override fun removeById(id: Int) {
        dbQuery.transaction {
            dbQuery.removeDocument(id.toLong())
        }
    }

    override fun getList(): Flow<List<Document>> =
        dbQuery.selectAllDocuments(::mapDocument).asFlow().mapToList()

    override fun getById(id: Int): Flow<Document> =
        dbQuery.selectDocumentById(id.toLong(), ::mapDocument).asFlow().mapToOne()

    override fun getLastId(): Int = dbQuery.selectLastDocumentId().executeAsOne().Max?.toInt() ?: 0

    private fun mapDocument(
        id: Long,
        name: String,
        status: Status
    ) =
        Document(
            id.toInt(), name, status
        )
}