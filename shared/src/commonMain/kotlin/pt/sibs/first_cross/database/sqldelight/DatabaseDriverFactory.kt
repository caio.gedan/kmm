package pt.sibs.first_cross.database.sqldelight

import com.squareup.sqldelight.db.SqlDriver

/**
 * Created by csilva at 06/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
expect class DatabaseDriverFactory {
    fun createDriver(): SqlDriver
}