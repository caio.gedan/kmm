package pt.sibs.first_cross.database

import com.squareup.sqldelight.ColumnAdapter
import pt.sibs.first_cross.database.sqldelight.DatabaseDriverFactory
import pt.sibs.first_cross.database.sqldelight.SDKDatabase
import pt.sibs.first_cross.domain.model.Status
import pt.sibs.firstcross.database.sqldelight.Document

/**
 * Created by csilva at 07/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */

fun database(databaseDriverFactory: DatabaseDriverFactory) = SDKDatabase.invoke(
    databaseDriverFactory.createDriver(),
    DocumentAdapter = Document.Adapter(statusAdapter)
)

val statusAdapter = object : ColumnAdapter<Status, String> {
    override fun decode(databaseValue: String): Status = when (databaseValue) {
        "Idle" -> Status.IDLE
        "Opened" -> Status.OPENED
        "Assigned" -> Status.ASSIGNED
        else -> Status.CLOSED
    }

    override fun encode(value: Status): String = when (value) {
        Status.IDLE -> "Idle"
        Status.OPENED -> "Opened"
        Status.ASSIGNED -> "Assigned"
        Status.CLOSED -> "Closed"
    }
}