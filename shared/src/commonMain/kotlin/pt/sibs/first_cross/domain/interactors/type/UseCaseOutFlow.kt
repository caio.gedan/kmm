package pt.sibs.first_cross.domain.interactors.type

import kotlinx.coroutines.flow.Flow

/**
 * Created by csilva at 06/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
abstract class UseCaseOutFlow<OUT> {
    abstract operator fun invoke(): Flow<OUT>
}