package pt.sibs.first_cross.domain.model.backend

import kotlinx.serialization.Serializable
import pt.sibs.first_cross.domain.model.Status

/**
 * Created by csilva at 06/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */

data class ApiDocumentsResponse(val results: List<ApiDocument>)

data class ApiDocument(
    @Serializable val id: Int? = null,
    @Serializable val name: String? = null,
    @Serializable val status: Status? = null
)