package pt.sibs.first_cross.domain

import kotlinx.coroutines.CoroutineDispatcher

/**
 * Created by csilva at 06/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
expect class MainDispatcher {
    val dispatcher: CoroutineDispatcher
}