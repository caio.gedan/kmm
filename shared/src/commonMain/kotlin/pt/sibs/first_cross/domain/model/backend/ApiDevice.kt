package pt.sibs.first_cross.domain.model.backend

import kotlinx.serialization.Serializable

/**
 * Created by csilva at 06/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
@Serializable
data class ApiDevice(
    @Serializable val systemFamily: String? = null,
    @Serializable val systemVersion: String? = null,
    @Serializable val systemArchitecture: String? = null,
    @Serializable val applicationName: String? = null,
    @Serializable val applicationVersion: String? = null,
    @Serializable val deviceManufacturer: String? = null,
    @Serializable val deviceModel: String? = null,
    @Serializable val ipAddress: String? = null,
    @Serializable val deviceId: String? = null,
)

