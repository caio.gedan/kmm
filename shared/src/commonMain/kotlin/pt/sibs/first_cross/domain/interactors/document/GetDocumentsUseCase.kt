package pt.sibs.first_cross.domain.interactors.document

import kotlinx.coroutines.flow.Flow
import pt.sibs.first_cross.domain.interactors.type.UseCaseOutFlow
import pt.sibs.first_cross.domain.model.Document
import pt.sibs.first_cross.services.main.IDocumentServices

/**
 * Created by csilva at 07/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
class GetDocumentsUseCase(
    private val docService: IDocumentServices
) : UseCaseOutFlow<List<Document>>() {
    override fun invoke(): Flow<List<Document>> = docService.getDBDocuments()
}