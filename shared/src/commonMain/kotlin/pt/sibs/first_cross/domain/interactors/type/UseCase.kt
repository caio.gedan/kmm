package pt.sibs.first_cross.domain.interactors.type

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

/**
 * Created by csilva at 06/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
abstract class UseCase {
    operator fun invoke(): Flow<Unit> = flow { emit(block()) }
    protected abstract val block: suspend () -> Unit
}