package pt.sibs.first_cross.domain.interactors.document

import pt.sibs.first_cross.domain.interactors.type.UseCaseIn
import pt.sibs.first_cross.domain.model.Document
import pt.sibs.first_cross.repository.model.mapper.ApiDocumentMapper
import pt.sibs.first_cross.services.main.IDocumentServices
import pt.sibs.first_cross.utils.helper.JsonBuilder

/**
 * Created by csilva at 06/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
class AssignDocumentUseCase(
    private val docService: IDocumentServices,
    private val docMapper: ApiDocumentMapper,
    override val block: suspend (param: Document) -> Unit = {
//        docService.assignDocument(JsonBuilder.toJson(docMapper.inverseMap(it)))
    }
) : UseCaseIn<Document>()