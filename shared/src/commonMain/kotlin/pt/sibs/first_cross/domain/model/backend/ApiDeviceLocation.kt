package pt.sibs.first_cross.domain.model.backend

import kotlinx.serialization.Serializable

/**
 * Created by csilva at 12/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
data class ApiDeviceLocation(
    @Serializable val latitude: String? = null,
    @Serializable val longitude: String? = null,
)

