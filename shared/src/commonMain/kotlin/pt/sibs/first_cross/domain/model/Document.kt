package pt.sibs.first_cross.domain.model

/**
 * Created by csilva at 07/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
data class Document(
    val id: Int,
    val name: String,
    val status: Status
)
