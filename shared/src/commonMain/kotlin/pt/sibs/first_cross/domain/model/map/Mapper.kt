package pt.sibs.first_cross.domain.model.map

/**
 * Created by csilva at 06/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
abstract class Mapper<M, P> {

    abstract fun mapToApi(model: M): P
    abstract fun mapFromApi(model: P): M

    fun mapToApi(values: List<M>): List<P> = values.map { mapToApi(it) }
    fun mapFromApi(values: List<P>): List<M> = values.map { mapFromApi(it) }
}