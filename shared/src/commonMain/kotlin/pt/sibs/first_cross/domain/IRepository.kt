package pt.sibs.first_cross.domain

import kotlinx.coroutines.flow.Flow

/**
 * Created by csilva at 07/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
interface IRepository<T> {
    fun addLocal(data: T)
    fun getLocalList(): Flow<List<T>>

    suspend fun getRemoteList(): List<T>
    suspend fun getFromRemote(id: Int): T
}