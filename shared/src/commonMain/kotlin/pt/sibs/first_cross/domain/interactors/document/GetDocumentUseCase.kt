package pt.sibs.first_cross.domain.interactors.document

import kotlinx.coroutines.flow.Flow
import pt.sibs.first_cross.domain.interactors.type.UseCaseInOutFlow
import pt.sibs.first_cross.domain.model.Document
import pt.sibs.first_cross.services.main.IDocumentServices

/**
 * Created by csilva at 06/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
class GetDocumentUseCase(
    private val docService: IDocumentServices,
) : UseCaseInOutFlow<Int, Document>() {
    override fun invoke(param: Int): Flow<Document> =
        docService.getDBDocument(param)
}
