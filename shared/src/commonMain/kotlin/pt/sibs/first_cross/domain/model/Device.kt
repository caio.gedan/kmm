package pt.sibs.first_cross.domain.model

/**
 * Created by csilva at 04/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */

data class Device(
    val systemFamily: String? = null,
    val systemVersion: String? = null,
    val systemArchitecture: String? = null,
    val deviceManufacturer: String? = null,
    val deviceModel: String? = null,
    val deviceId: String? = null,
)

data class DeviceLocation(
    val latitude: String? = null,
    var longitude: String? = null,
)