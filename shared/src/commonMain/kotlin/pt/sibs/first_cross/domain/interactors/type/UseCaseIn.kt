package pt.sibs.first_cross.domain.interactors.type

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

/**
 * Created by csilva at 06/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
abstract class UseCaseIn<IN> {
    operator fun invoke(param: IN): Flow<Unit> = flow { emit(block(param)) }
    protected abstract val block: suspend (param: IN) -> Unit
}