package pt.sibs.first_cross.domain.interactors.document

import pt.sibs.first_cross.domain.interactors.type.UseCaseIn
import pt.sibs.first_cross.domain.model.Document
import pt.sibs.first_cross.repository.IDocumentRepository

/**
 * Created by csilva at 06/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
class SaveDocumentUseCase(
    private val repository: IDocumentRepository,
    override val block: suspend (param: Document) -> Unit = { repository.addLocal(it) }
) : UseCaseIn<Document>()