package pt.sibs.first_cross.base.executor

/**
 * Created by csilva at 06/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
interface IExecutorScope {
    fun cancel()
}