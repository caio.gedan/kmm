package pt.sibs.first_cross.base.mvi

/**
 * Created by csilva at 08/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
sealed class BasicUiState<out T> {
    data class Success<T>(val data: T) : BasicUiState<T>()
    data class Error(val message: String? = null) : BasicUiState<Nothing>()
    object Loading : BasicUiState<Nothing>()
    object Empty : BasicUiState<Nothing>()
    object Idle : BasicUiState<Nothing>()
}