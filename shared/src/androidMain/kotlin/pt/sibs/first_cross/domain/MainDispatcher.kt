package pt.sibs.first_cross.domain

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

/**
 * Created by csilva at 06/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
actual class MainDispatcher {
    actual val dispatcher: CoroutineDispatcher = Dispatchers.Default
}