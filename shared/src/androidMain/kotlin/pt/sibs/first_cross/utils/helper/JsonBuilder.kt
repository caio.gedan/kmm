package pt.sibs.first_cross.utils.helper

import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder

/**
 * Created by csilva at 04/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
/**
 * Created by csilva at 04/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */

actual object JsonBuilder {

    private val gsonBuilder = GsonBuilder()
    private var gson = Gson()

    init {
        gsonBuilder.setFieldNamingStrategy(FieldNamingPolicy.IDENTITY)
        gson = gsonBuilder.serializeNulls().create()
    }

    actual fun toJson(pojo: Any): String = gson.toJson(pojo)

    actual fun <T> fromJson(json: String, type: Class<T>): T {
        return gson.fromJson(json, type)

    }

}