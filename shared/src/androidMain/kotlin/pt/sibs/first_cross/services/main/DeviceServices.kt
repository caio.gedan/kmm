package pt.sibs.first_cross.services.main

import android.os.Build
import pt.sibs.first_cross.domain.model.Device
import pt.sibs.first_cross.domain.model.DeviceLocation
import pt.sibs.first_cross.services.base.IServiceContract
import pt.sibs.first_cross.utils.helper.JsonBuilder
import java.util.*

/**
 * Created by csilva at 04/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
actual object DeviceServices {

    val deviceInfo =
        object : IServiceContract {
            override fun process(): IServiceContract.Response {
                return IServiceContract.Response.Success(JsonBuilder.toJson(getDeviceInfo()))
            }
        }

    val deviceLocation = object : IServiceContract {
        override fun process(): IServiceContract.Response {
            return IServiceContract.Response.Success(getDeviceLocation())
        }
    }

    private fun getDeviceInfo() =
        Device(
            "Android",
            Arrays.toString(Build.SUPPORTED_ABIS),
            Build.VERSION.RELEASE + "API LEVEL" + Build.VERSION.SDK_INT,
            Build.MODEL,
            Build.MANUFACTURER,
            "Algum ID"
        )

    private fun getDeviceLocation() = DeviceLocation("37.987", "9.2321")
}

