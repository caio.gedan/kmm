package pt.sibs.first_cross.services

import android.util.Log
import androidx.annotation.Keep
import org.koin.java.KoinJavaComponent
import pt.sibs.first_cross.services.provider.IServiceProvider

/**
 * Created by csilva at 12/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
actual class CppBridge {

    companion object CPP {
        private const val TAG = "CppBridge"

        private val srv: IServiceProvider by KoinJavaComponent.inject(IServiceProvider::class.java)

        external fun startTest()

        @Keep
        external fun onClientResponseSync(response: String)

        @Keep
        external fun onClientResponseAsync(response: String)

        @JvmStatic
        fun onServerResponse(response: String, req: String) {
            Log.i(TAG, "Called :: onCallbackResponse($response)")

        }

        @JvmStatic
        fun executeSyncCallback(uri: String, req: String) {
            Log.i(TAG, "Called :: executeSyncCallback($uri)")
            srv.executeSync(uri, req) {
                onClientResponseSync(it)
            }
        }

        @JvmStatic
        fun executeSync(uri: String, req: String) {
            Log.i(TAG, "Called :: executeSync($uri)")
            srv.executeSync(uri, req)
        }

        @JvmStatic
        fun executeSyncWithResponse(uri: String, req: String): String =
            srv.executeSyncWithResponse(uri, req)

        @JvmStatic
        fun executeAsyncCallback(uri: String, req: String) {
            Log.i(TAG, "Called :: executeAsyncCallback($uri)")
            srv.executeAsync(uri, req) {
                onClientResponseAsync(it)
            }
        }

        @JvmStatic
        fun executeAsync(uri: String, req: String) {
            Log.i(TAG, "Called :: executeAsync($uri)")
            srv.executeAsync(uri, req)
        }

        init {
            System.loadLibrary("first_lib_Native")
        }
    }
}