package pt.sibs.first_cross.di

import org.koin.core.module.Module
import org.koin.dsl.module
import pt.sibs.first_cross.database.sqldelight.DatabaseDriverFactory
import pt.sibs.first_cross.domain.MainDispatcher

/**
 * Created by csilva at 03/05/2022.
 * SIBS :: DTDDM
 * caio.silva@sibs.com
 */
actual fun platformModule(): Module =
    module {
        single { DatabaseDriverFactory(get()) }
        single { MainDispatcher() }
    }
