plugins {
    kotlin("multiplatform")
    id("com.android.library")
    id("kotlin-android-extensions")
    id("com.squareup.sqldelight")
}

kotlin {
    android()
//    val iosTarget: (String, KotlinNativeTarget.() -> Unit) -> KotlinNativeTarget =
//        if (System.getenv("SDK_NAME")?.startsWith("iphoneos") == true)
//            ::iosArm64
//        else
//            ::iosX64
//
//    iosTarget("ios") {}

    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation("io.insert-koin:koin-core:3.1.2")
                implementation("com.squareup.sqldelight:runtime:1.5.3")
                implementation("com.squareup.sqldelight:coroutines-extensions:1.5.3")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-core:1.2.2")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.1.0")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.1-native-mt") {
                    version {
                        strictly("1.6.1-native-mt")
                    }
                }
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
            }
        }

        val androidMain by getting {
            dependencies {
                implementation("androidx.core:core-ktx:1.7.0")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.6.1")
                //Koin
                implementation("io.insert-koin:koin-core:3.1.2")
                implementation("io.insert-koin:koin-android:3.1.2")
                //Gson
                implementation("com.google.code.gson:gson:2.8.9")
                //SqlDelight
                implementation("com.squareup.sqldelight:android-driver:1.5.3")

            }
        }

        val androidTest by getting {
            dependencies {
                implementation(kotlin("test-junit"))
                implementation("junit:junit:4.13.2")
            }
        }

//        val iosMain by getting {
//            dependencies {
//
//            }
//        }
//        val iosTest by getting

    }

}

android {

    compileSdk = 28
    sourceSets["main"].manifest.srcFile("src/androidMain/AndroidManifest.xml")

    defaultConfig {
        minSdk = 21
        targetSdk = 28
        version = 1
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

        externalNativeBuild {
            cmake {
                cppFlags("-frtti -fexceptions -g2 -O3 -std=c++14 -v -D \"USE_PRECOMPILED_HEADERS\" -D \"RAPIDJSON_HAS_STDSTRING\"")
                arguments(
                    "-DANDROID_TOOLCHAIN=clang",
                    "-DANDROID_STL=gnustl_shared",
                    "-DANDROID_STL=c++_shared",
                    "-DCMAKE_VERBOSE_MAKEFILE=ON"
                )
            }
        }
//        ndk {
//            abiFilters.addAll(mutableListOf("armeabi-v7a", "x86", "x86_64"))
//        }
    }

    externalNativeBuild {
        cmake {
            path("servicebridge/CMakeLists.txt")
            version = "3.18.1"
        }
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = true
        }
        getByName("debug") {
            isMinifyEnabled = false
        }
    }
}

//afterEvaluate {
//    tasks.named("compileCommonMainKotlinMetadata").configure {
//        // https://youtrack.jetbrains.com/issue/KT-51293
//        enabled = false
//    }
//}

//https://github.com/cashapp/sqldelight
sqldelight {
    database("SDKDatabase") {
        packageName = "pt.sibs.first_cross.database.sqldelight"
        sourceFolders = listOf("kotlin")
    }
    linkSqlite = true
}
