//
// Created by csilva on 03/05/2022.
//

#include <mutex>
#include <jni.h>
#include <string>
#include <thread>
#include <iostream>
#include <android/log.h>

using namespace std;

void getStringFromJString(JNIEnv *env, jstring jstr, std::string &str) {
    const char *_str = env->GetStringUTFChars(jstr, 0);
    str = _str;
    env->ReleaseStringUTFChars(jstr, _str);
}

void waitFor(int delayMs)
{
    std::this_thread::sleep_for(std::chrono::milliseconds(delayMs));
}

extern "C" {
jclass jHandlerClass;
static pthread_key_t mThreadKey;
static JavaVM *jvm;

static void Android_JNI_ThreadDestroyed(void *value)
{
    /* The thread is being destroyed, detach it from the Java VM and set the mThreadKey value to NULL as required */
    auto *env = (JNIEnv *) value;
    if (env != nullptr)
    {
        jvm->DetachCurrentThread();
        pthread_setspecific(mThreadKey, nullptr);
    }
}

static JNIEnv *Android_JNI_GetEnv()
{
    JNIEnv *gEnv;

    jvm->GetEnv((void **) &gEnv, JNI_VERSION_1_6);
    if (gEnv == nullptr)
    {

        int status = jvm->AttachCurrentThread(&gEnv, nullptr);
        pthread_key_create(&mThreadKey, Android_JNI_ThreadDestroyed);
        pthread_setspecific(mThreadKey, (void *) gEnv);
        if (status < 0)
        {
            return 0;
        }
    }

    return gEnv;
}

void convertStringToJstring(JNIEnv *env, jstring &jstr, std::string message)
{
    JNIEnv *g_env = Android_JNI_GetEnv();

    if (g_env != nullptr)
    {
        jstr = g_env->NewStringUTF(message.c_str());
    }
}

std::string executeSyncWithResponse(const std::string& uri, const std::string& req)
{
    JNIEnv *g_env = Android_JNI_GetEnv();

    jmethodID jHandlerExecuteSyncResponse = g_env->GetStaticMethodID(jHandlerClass, "executeSyncWithResponse",
                                                            "(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;");

    jstring uriValue = g_env->NewStringUTF((const char *) uri.c_str());
    jstring requestValue = g_env->NewStringUTF((const char *) req.c_str());

    auto str = (jstring) g_env->CallStaticObjectMethod(jHandlerClass, jHandlerExecuteSyncResponse, uriValue, requestValue);

    std::string respString;

    if (str != NULL)
    {
        getStringFromJString(g_env, str, respString);
    }

    g_env->DeleteLocalRef((jobject) str);
    g_env->DeleteLocalRef((jobject) uriValue);
    g_env->DeleteLocalRef((jobject) requestValue);

    return respString;
}

void executeSyncCallback(const std::string& uri, const std::string& req)
{
    JNIEnv *g_env = Android_JNI_GetEnv();

    jmethodID jHandlerExecuteAsync = g_env->GetStaticMethodID(jHandlerClass, "executeSyncCallback",
                                                              "(Ljava/lang/String;Ljava/lang/String;)V");

    jstring uriValue = g_env->NewStringUTF((const char *) uri.c_str());
    jstring requestValue = g_env->NewStringUTF((const char *) req.c_str());

    g_env->CallStaticVoidMethod(jHandlerClass, jHandlerExecuteAsync, uriValue, requestValue);

    g_env->DeleteLocalRef((jobject) uriValue);
    g_env->DeleteLocalRef((jobject) requestValue);
}

void executeAsyncCallback(const std::string& uri, const std::string& req)
{
    JNIEnv *g_env = Android_JNI_GetEnv();

    jmethodID jHandlerExecuteAsync = g_env->GetStaticMethodID(jHandlerClass, "executeAsyncCallback",
                                                                     "(Ljava/lang/String;Ljava/lang/String;)V");

    jstring uriValue = g_env->NewStringUTF((const char *) uri.c_str());
    jstring requestValue = g_env->NewStringUTF((const char *) req.c_str());

    g_env->CallStaticVoidMethod(jHandlerClass, jHandlerExecuteAsync, uriValue, requestValue);

    g_env->DeleteLocalRef((jobject) uriValue);
    g_env->DeleteLocalRef((jobject) requestValue);
}

void executeAsync(const std::string& uri, const std::string& req)
{
    JNIEnv *g_env = Android_JNI_GetEnv();

    jmethodID jHandlerExecuteAsync = g_env->GetStaticMethodID(jHandlerClass, "executeAsync",
                                                              "(Ljava/lang/String;Ljava/lang/String;)V");

    jstring uriValue = g_env->NewStringUTF((const char *) uri.c_str());
    jstring requestValue = g_env->NewStringUTF((const char *) req.c_str());

    g_env->CallStaticVoidMethod(jHandlerClass, jHandlerExecuteAsync, uriValue, requestValue);

    g_env->DeleteLocalRef((jobject) uriValue);
    g_env->DeleteLocalRef((jobject) requestValue);
}

void executeSync(const std::string& uri, const std::string& req)
{
    JNIEnv *g_env = Android_JNI_GetEnv();

    jmethodID jHandlerExecuteAsync = g_env->GetStaticMethodID(jHandlerClass, "executeSync",
                                                              "(Ljava/lang/String;Ljava/lang/String;)V");

    jstring uriValue = g_env->NewStringUTF((const char *) uri.c_str());
    jstring requestValue = g_env->NewStringUTF((const char *) req.c_str());

    g_env->CallStaticVoidMethod(jHandlerClass, jHandlerExecuteAsync, uriValue, requestValue);

    g_env->DeleteLocalRef((jobject) uriValue);
    g_env->DeleteLocalRef((jobject) requestValue);
}

/*
 * Class:     Java_pt_sibs_first_1cross_services_CppBridge_00024CPP
 * Method:    startTest
 * Signature: ()V
 */
JNIEXPORT void JNICALL
Java_pt_sibs_first_1cross_services_CppBridge_00024CPP_startTest(JNIEnv *env, jobject instance) {
    std::string serviceUri = "device/info";
    std::string reqValue = R"({"applicationName":null,"applicationVersion":null,"deviceId":null,"deviceManufacturer":null,"deviceModel":null,"ipAddress":null,"systemArchitecture":null,"systemFamily":null,"systemVersion":null})";

    std::string res = executeSyncWithResponse(serviceUri, reqValue);
    waitFor(5000);

    __android_log_print(ANDROID_LOG_INFO, "C++ RESPONSE", "%s", res.c_str());
}

/*
 * Class:     Java_pt_sibs_first_1cross_services_CppBridge_00024CPP
 * Method:    onClientResponseSync
 * Signature: (jstring)void;
 */
JNIEXPORT void JNICALL
Java_pt_sibs_first_1cross_services_CppBridge_00024CPP_onClientResponseSync(JNIEnv *env, jobject instance,
                                                                           jstring response) {
    std::string res;
    getStringFromJString(env, response, res);
    waitFor(5000);

//    __android_log_print(ANDROID_LOG_INFO, "C++ RESPONSE", "%s", res.c_str());
}

/*
 * Class:     Java_pt_sibs_first_1cross_services_CppBridge_00024CPP
 * Method:    onClientResponseAsync
 * Signature: (jstring)void;
 */
JNIEXPORT void JNICALL
Java_pt_sibs_first_1cross_services_CppBridge_00024CPP_onClientResponseAsync(JNIEnv *env, jobject instance,
                                                                            jstring response) {
    std::string res;
    getStringFromJString(env, response, res);
    waitFor(5000);

//    __android_log_print(ANDROID_LOG_INFO, "C++ RESPONSE", "%s", res.c_str());
}

//https://developer.android.com/training/articles/perf-jni
JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM *vm, void *reserved)
{

    JNIEnv *env;
    jvm = vm;

    if (jvm->GetEnv(reinterpret_cast<void **>(&env), JNI_VERSION_1_6) != JNI_OK)
    {
        return JNI_ERR;
    }

    // Find your class. JNI_OnLoad is called from the correct class loader context for this to work.
    jclass clz = env->FindClass("pt/sibs/first_cross/services/CppBridge");
    if (clz == nullptr) return JNI_ERR;

    jHandlerClass = reinterpret_cast<jclass>(env->NewGlobalRef(clz));

    return JNI_VERSION_1_6;
}

}


